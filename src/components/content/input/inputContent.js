import { Component } from "react";

const onClickHandler = () => {
    console.log("Đã bấm nút gửi thông điệp");
}
class InputContent extends Component {
    
    render() {
        return (
            <>
                <div className="mt-3">
                    <label>Message cho bạn 12 tháng tới</label>
                </div>
                <div className="mt-3">
                    <input className="form-control"></input>
                </div>
                <div className="mt-3">
                    <button onClick={onClickHandler} className="btn btn-primary">Gửi thông điệp</button>
                </div>
            </>

        )
    }
}

export default InputContent;