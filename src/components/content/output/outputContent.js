import { Component } from "react";
import icon from "../../../assets/images/likeicon.png"

class OutputContent extends Component {
    render() {
        return (
            <>
                <div className="mt-3">
                    <p>Nội dung thông điệp</p>
                </div>
                <div className="mt-3">
                    <img src={icon} alt="Devcamp120" width="50" />
                </div>
            </>

        )
    }
}

export default OutputContent;